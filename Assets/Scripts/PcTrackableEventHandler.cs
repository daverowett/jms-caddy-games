﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCTrackableEventHandler : DefaultTrackableEventHandler
{

    //private PCController controller;
    private void Awake()
    {
        //controller = GetComponentInChildren<PCController>();
    }

    protected override void OnTrackingFound()
    {
        //Activate the time on the image 
        Debug.Log("OVERRIDE OnTrackingFound " + this.name);

        base.OnTrackingFound();

        //controller.Found();
    }

    protected override void OnTrackingLost()
    {
        Debug.Log("OVERRIDE OnTrackingLost " + this.name);

        base.OnTrackingLost();
        //controller.Lost();
    }
}
