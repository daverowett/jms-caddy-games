﻿using System;


namespace Assets.Scripts.Classes
{
    [System.Serializable]
    public class BackgroundModel
    {
        [UnityEngine.SerializeField]
        public UnityEngine.Color color { get; set; }
    }

    [System.Serializable]
    public class CopyAndBackgroundModel : BackgroundModel
    {
        [UnityEngine.SerializeField]
        public string copy { get; set; }
        [UnityEngine.SerializeField]
        public UnityEngine.Color? copyColor { get; set; }
    }
}
