﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCActivationController : MonoBehaviour {

    public GameObject _activationAnimation;

    private void Awake()
    {
        Debug.Log(this.name + " Awake!");
    }

    public void Start()
    {
        SetActivationAnimationActive(false);
    }

    public void SetActivationAnimationActive(bool status)
    {
        _activationAnimation.SetActive(status);
    }
}
