﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppController : MonoBehaviour {

    //Maybe a Queue for first in first out processing, but idk yet... how do we remove a specific item from a queue
    private Queue<PCController> ActivePCQueue = new Queue<PCController>(); // Queue is the current list of active pcs
    private PCController ActivePC;

    public enum PCControllerMessaging { Activate, MultiplePCs, Inactivate, OtherActive }

    public void Awake()
    {
        
    }

    public void Update()
    {
        IsEscape();
    }

    public void EnqueueActivePCQueue(PCController controller)
    {
        if (!ActivePCQueue.Contains(controller))
            ActivePCQueue.Enqueue(controller);
    }

    public void DequeueActivePCQueue()
    {
        
    }

    public PCControllerMessaging CanIBeActivePC(PCController controller)
    {
        //There is more here we need to notify the pc which state we are in
        // you can activate,
        // There are multiples on screen
        // we have already an active controller

        PCControllerMessaging message = PCControllerMessaging.OtherActive;

        if (ActivePC == null)
        {
            if (ActivePCQueue.Count == 1)
            {
                ActivePC = controller;
                message = PCControllerMessaging.Activate;
            }
            else
                message = PCControllerMessaging.MultiplePCs;
        }

        return message;
    }

    //when a queue item is removed we need to notify each of the ActivePCQueues... This should be done with static events...
    // I will updates when i get this worked out.

    private void IsEscape()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
                activity.Call<bool>("moveTaskToBack", true);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
