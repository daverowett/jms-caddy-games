﻿using UnityEngine;

public static class Easing
{
    public static float EaseOut(float currentLerpTime, float lerpTime)
    {
        float t = currentLerpTime / lerpTime;

        return Mathf.Sin(t * Mathf.PI * 0.5f);
    }

    public static float EaseIn(float currentLerpTime, float lerpTime)
    {
        float t = currentLerpTime / lerpTime;

        return 1f - Mathf.Cos(t * Mathf.PI * 0.5f);
    }

    public static float Exponential(float currentLerpTime, float lerpTime)
    {
        float t = currentLerpTime / lerpTime;

        return t * t;
    }

    public static float SmoothStep(float currentLerpTime, float lerpTime)
    {
        float t = currentLerpTime / lerpTime;

        return t * t * (3f - 2f * t);
    }

    public static float SmootherStep(float currentLerpTime, float lerpTime)
    {
        float t = currentLerpTime / lerpTime;

        return t * t * t * (t * (6f * t - 15f) + 10f);
    }
}