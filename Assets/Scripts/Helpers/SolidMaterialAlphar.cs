﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BSolidMaterialAlphar : MonoBehaviour {

    private Renderer rend;
    private float StartAlpha { get; set; }
    private float NextAlpha { get; set; }
    private float CurrentAlphaTime { get; set; }
    public float alphaTime { get; set; }

    private void Awake ()
    {
        rend = GetComponent<Renderer>();

        CurrentAlphaTime = 0f;
        alphaTime = 1f;
        StartAlpha = rend.material.color.a;
        NextAlpha = rend.material.color.a;
	}
	
	// Update is called once per frame
	private void Update ()
    {
		if(StartAlpha != NextAlpha)
        {
            CurrentAlphaTime += Time.deltaTime;

            if (CurrentAlphaTime > alphaTime)
                CurrentAlphaTime = alphaTime;

            Color color = rend.material.color;

            color.a = Mathf.Lerp(StartAlpha, NextAlpha, Easing.EaseIn(CurrentAlphaTime, alphaTime));

            rend.material.color = color;

            if (NextAlpha == rend.material.color.a)
                StartAlpha = NextAlpha;
        }
	}

    public void SetNextAlphaAndTime (float alpha, float time = 1f)
    {
        NextAlpha = alpha;
        alphaTime = time;
    }
}
