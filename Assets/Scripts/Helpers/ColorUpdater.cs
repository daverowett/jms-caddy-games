﻿using UnityEngine;

public class ColorUpdater : MonoBehaviour {

    private Renderer Rend;
    private Color Color = Color.red;


    private void Awake ()
    {

        Rend = GetComponent<Renderer>();
    }

    private void Update()
    {

    }

    public void UpdateColor(Color color)
    {
        Rend.material.color = color;
    }
}
