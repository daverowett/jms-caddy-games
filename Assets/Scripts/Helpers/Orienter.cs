﻿using UnityEngine;

public class Orienter : MonoBehaviour {

    private Quaternion StartOrientation { get; set; }

    private Quaternion NextOrientation { get; set; }

    private float CurrentOrientationTime { get; set; }

    private float OrientationTime { get; set; }

    private void Awake()
    {
        CurrentOrientationTime = 0f;
        OrientationTime = 1f;

        ResetOrientations();
    }
    
    private void Update()
    { 
        if (StartOrientation != NextOrientation)
        {
            CurrentOrientationTime += Time.deltaTime;

            if (CurrentOrientationTime > OrientationTime)
                CurrentOrientationTime = OrientationTime;

            transform.localRotation = Quaternion.Slerp(StartOrientation, NextOrientation, Easing.EaseIn(CurrentOrientationTime, OrientationTime));

            if (NextOrientation == transform.localRotation)
                ResetOrientations();
        }
    }

    private void ResetOrientations()
    {
        StartOrientation = transform.localRotation;
        NextOrientation = transform.localRotation;
    }

    public void UpdateRotations(Vector3 rotate)
    {
        transform.Rotate(rotate);
    }

    //Please use Quaternion.Euler and not a native quaternion
    public void SetNextRotationAndTime(Quaternion rotation, float time = 1f)
    {
        NextOrientation *= rotation;
        OrientationTime = time;
    }
}
