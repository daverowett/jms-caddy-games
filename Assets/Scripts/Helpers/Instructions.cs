﻿using System;

namespace Assets.Scripts
{
    //This houses the available states of the instructions ui overlay
    // The overlay instance will have the current state and can convert it to copy for display when needed.
    public static class Instructions
    {
        public static readonly string PreAciveOnly1 = "Only one pc can be active at a time";
        public static readonly string PostActiveVisibleInactive = "Use the back button to reset select a new pc";
        public static readonly string MultipleNonActivePCS = "Only 1 type of pc can be active";
        public static readonly string TapToActivate = "Tap Lid to activate";
    }
}
