﻿using System.Collections;
using UnityEngine;

public class PCController : MonoBehaviour
{
    public AppController AppController;
    public GameObject PcMessaging;
    public GameObject PcActivation;

    //What fruit the pc will produce
    public enum FruitType { Strawberry, Grape }
    [SerializeField]
    public FruitType fruitType;

    //Ready produces fruit instantly
    //Prompt gets verification before producing fruit.
    public enum ProductionType { Ready, LaunchAnyway }
    [SerializeField]
    public ProductionType productionType;

    //These are states
    private bool IsVisible;         //Is this PC Currently on the screen
    private bool IsPlayingGame;     //tapped and a game is running
    private bool IsActive;          //ready to tap
    private bool IsLaunchAnyway;    //SpeachBubble and launchAnyway
    private bool IsTooManyPCs;      //multiple pc visible
    
    private PCMessagingController _pcMessagingController;
    private PCActivationController _pcActivationController;

    public void Awake()
    {
        ResetState();

        _pcMessagingController = PcMessaging.GetComponentInChildren<PCMessagingController>();
        _pcActivationController = PcActivation.GetComponentInChildren<PCActivationController>();
    }

    public void Start()
    {
        //UpdateChildrenEnabled(false);
    }
    
    private void UpdateChildrenEnabled(bool status)
    {
        //PcMessaging.SetActive(status);
        //PcActivation.SetActive(status);
        
    }

    public void Found()
    {
        Debug.Log("Found:" + transform.parent.name);
    }

    public void Lost()
    {
        Debug.Log("Lost:" + transform.parent.name);
    }

    public SetMessagingFor()

    //so here we call the end point to show this pc now has be recognized 
    public void PrepActivations()
    {
        AppController.EnqueueActivePCQueue(this);

        StartCoroutine(AreWeReady());
    }

    private void ResetState()
    {
        IsVisible = false;
        IsPlayingGame = false;
        IsActive = false;
        IsLaunchAnyway = false;
        IsTooManyPCs = false;
    }

    IEnumerator AreWeReady()
    {
        yield return new WaitForSeconds(1f);

        var state = AppController.CanIBeActivePC(this);
        Debug.Log(state);
        switch (state)
        {
            case AppController.PCControllerMessaging.Activate:
                break;
            case AppController.PCControllerMessaging.Inactivate:
                break;
            case AppController.PCControllerMessaging.MultiplePCs:
                break;
            case AppController.PCControllerMessaging.OtherActive:
            default:
                break;
        }
    }
}