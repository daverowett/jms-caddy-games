﻿using UnityEngine;
using Assets.Scripts.Classes;

public class PCMessagingController : MonoBehaviour
{
    
    [SerializeField]
    public GameObject _pcCover;
    [SerializeField]
    public GameObject _speechBubble;
    [SerializeField]
    public GameObject _launchAnyway;
    
    public CopyAndBackgroundModel CoverModel { get; set; }
    public CopyAndBackgroundModel speechBubbleModel { get; set; }
    public CopyAndBackgroundModel launchAnyway { get; set; }

    private CopyAndBackgroundController _pcCoverCBC;
    private CopyAndBackgroundController _speechBubbleCBC;
    private CopyAndBackgroundController _launchAnywayCBC;

    private void Awake()
    {
        _pcCoverCBC = _pcCover.GetComponentInChildren<CopyAndBackgroundController>();
        _speechBubbleCBC = _speechBubble.GetComponentInChildren<CopyAndBackgroundController>();
        _launchAnywayCBC = _launchAnyway.GetComponentInChildren<CopyAndBackgroundController>();
    }

    private void Start()
    {
        UpdateChildrenEnabled(false);
        //CopyAndBackgroundController Testing
        //UpdatePCCover(_coverColor, _coverCopy, _coverCopyColor);
        //UpdateSpeechBubble(_speechBubbleColor, _speechBubbleCopy, _speechBubbleCopyColor);
        //UpdateLaunchAnyway(_launchAnywayColor, _launchAnywayCopy, _launchAnywayCopyColor);

        //rotational testing use orienter
        //_pcCover.GetComponent<Orienter>().UpdateRotations(new Vector3(0,0,180));
        //_pcCover.GetComponent<Rotator>().SetNextRotationAndTime(Quaternion.Euler(0, 0, 90), 2f);
        //_pcCover.GetComponent<Orienter>().SetNextRotationAndTime(Quaternion.Euler(0, 0, 270), .5f);
    }

    public void UpdateChildrenEnabled(bool status)
    {
        SetPCCoverActive(status);
        SetSpeechBubbleActive(status);
        SetLauchAnywayActive(status);
    }
    
    public void SetPCCoverActive(bool status)
    {
        _pcCover.SetActive(status);
    }

    public void SetSpeechBubbleActive(bool status)
    {
        _speechBubble.SetActive(status);
    }

    public void SetLauchAnywayActive(bool status)
    {
        _launchAnyway.SetActive(status);
    }

    public void UpdatePCCover(CopyAndBackgroundModel model)
    {
        _pcCoverCBC.SetCover(model);
    }

    public void UpdateSpeechBubble(CopyAndBackgroundModel model)
    {
        _speechBubbleCBC.SetCover(model);
    }

    public void UpdateLaunchAnyway(CopyAndBackgroundModel model)
    {
        _launchAnywayCBC.SetCover(model);
    }
}
