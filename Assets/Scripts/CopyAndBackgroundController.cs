﻿using UnityEngine;
using Assets.Scripts.Classes;

using TMPro;
public class CopyAndBackgroundController : MonoBehaviour
{
    private ColorUpdater _cover;
    private TextMeshPro _copy;

    private void Awake()
    {
        _cover = GetComponentInChildren<ColorUpdater>();
        _copy = GetComponentInChildren<TextMeshPro>();
    }
   
    public void SetCover(CopyAndBackgroundModel model)
    {
        _cover.UpdateColor(model.color);
        _copy.text = model.copy.Replace("\\n", "\n");
        _copy.color = model.copyColor ?? Color.white;
    }
}